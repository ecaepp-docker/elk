#! /bin/bash

git pull git@gitlab.com:ecaepp-docker/elk.git

declare -a arr=("docker.elastic.co/elasticsearch/elasticsearch:6.6.1"
                "docker.elastic.co/kibana/kibana:6.6.1"
                "docker.elastic.co/logstash/logstash:6.4.3")

for i in ${arr[@]}
do
    docker pull "$i"
done

docker stack deploy -c docker-compose.yml elk
